package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.*;

@Remote
public interface CandidateServiceRemote {

	public Candidate DisplayCandidate (int id);
	public List<Candidate> getAllCandidate ();
	public Candidate getByEmailPwd (String email, String pwd); 
}
