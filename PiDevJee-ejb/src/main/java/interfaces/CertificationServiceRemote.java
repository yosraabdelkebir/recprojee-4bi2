package interfaces;

import java.util.List;
import javax.ejb.Remote;

import model.*;

@Remote
public interface CertificationServiceRemote {

	public Certification DisplayCertification (int id);
	public List<Certification> GetAllCertifofCandidate(int idC);
}
