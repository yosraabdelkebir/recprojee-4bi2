package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.*;

@Remote
public interface ContactServiceRemote {
	
	public List<Contact> getAllContact();
	public List<Contact> getContactofCand(int id);
	public List<Candidate> getAmisCommun(int idX ,int idY);
	public List<Candidate> getAmisDesAmis(int idC);

}
