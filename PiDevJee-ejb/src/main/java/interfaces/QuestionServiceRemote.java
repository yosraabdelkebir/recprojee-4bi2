package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.Question;
import model.Response;

@Remote
public interface QuestionServiceRemote {
	
	public Question addQuestion(Question q);
	public void updateQuestion(Question r);
	public void removeQuestion(int id);
	public List<Question> getAllQuestions();

}
