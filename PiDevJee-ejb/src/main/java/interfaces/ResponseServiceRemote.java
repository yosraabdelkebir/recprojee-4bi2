package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.OnLineTest;
import model.Response;

@Remote
public interface ResponseServiceRemote {
	
	public int addResponse(Response r);
	public void updateResponse(Response r);
	public void removeResponse(int id);
	public List<Response> getResponseByQuestion(int QuestionId);

}
