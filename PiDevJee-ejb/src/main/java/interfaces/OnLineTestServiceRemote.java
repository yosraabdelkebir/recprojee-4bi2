package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.OnLineTest;

@Remote
public interface OnLineTestServiceRemote {
	public List<OnLineTest> getNonPassedTestByCandidate (int candidateId);
	public OnLineTest getOnlineTest(int id);
	public void updateOnLineTest(OnLineTest o);
	public void sendMail (String receiver, String text);
	
}
