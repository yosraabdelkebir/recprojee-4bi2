package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.*;

@Remote
public interface QuizServiceRemote {
	
	public List<Question> createQuiz(int JobOfferId);

}
