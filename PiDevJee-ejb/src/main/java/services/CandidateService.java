package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import interfaces.CandidateServiceLocal;
import interfaces.CandidateServiceRemote;
import model.Candidate;

@Stateless
@LocalBean
public class CandidateService implements CandidateServiceLocal, CandidateServiceRemote {
	
	@PersistenceContext
	EntityManager em;

	@Override
	public Candidate DisplayCandidate(int id) {
		//System.out.println("****************"+em.find(Candidate.class, id).getEmail());
		TypedQuery<Candidate> query =  em.createQuery("Select c from Candidate c WHERE c.id=:id", Candidate.class);
		query.setParameter("id", id); 
		Candidate candidat = null; 
		try {
			candidat = query.getSingleResult(); 
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return candidat;
	}

	@Override
	public Candidate getByEmailPwd(String email, String pwd) {
		TypedQuery<Candidate> query = 
				em.createQuery("select c from Candidate c WHERE c.email=:email and c.password=:pwd ", Candidate.class); 
				query.setParameter("email", email); 
				query.setParameter("pwd", pwd); 
				Candidate candidat = null; 
				try {
					candidat = query.getSingleResult(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				return candidat;
	}

	@Override
	public List<Candidate> getAllCandidate() {
		List<Candidate> candidats = em.createQuery("Select c from Candidate c ", Candidate.class).getResultList();
		
		return candidats;
	}

}
