package services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.QuestionServiceLocal;
import interfaces.QuestionServiceRemote;
import model.Question;
import model.Response;

@Stateless
@LocalBean
public class QuestionService implements QuestionServiceLocal, QuestionServiceRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@EJB
	ResponseService SResp = null;
	
	@Override
	public Question addQuestion(Question q) {
		em.persist(q);
		return q;
	}

	@Override
	public List<Question> getAllQuestions() {
		return em.createQuery("Select q from Question q ", Question.class).getResultList();
	}

	@Override
	public void updateQuestion(Question r) {
		em.merge(r);
		System.out.println("Question updated !!!!!!! ");
		
	}

	@Override
	public void removeQuestion(int id) {
		List<Response> toremove = SResp.getResponseByQuestion(id);
		for (Response r : toremove)  SResp.removeResponse(r.getResponseId());
		em.remove(em.find(Question.class,id));
		System.out.println("Question removed !!!!!!! ");
		
	}

}
