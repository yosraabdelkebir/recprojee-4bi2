package services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.QuizServiceLocal;
import interfaces.QuizServiceRemote;
import model.JobOffer;
import model.Question;

@Stateless
@LocalBean
public class QuizService implements QuizServiceLocal, QuizServiceRemote {
	
	@PersistenceContext
	EntityManager em;

	@Override
	public List<Question> createQuiz(int JobOfferId) {
		List<Question> all = em.createQuery("Select q from Question q ", Question.class).getResultList();
		List<Question> byTheme = new ArrayList<Question>();
		List<Question> culture = new ArrayList<Question>();
		List<Question> personnalite = new ArrayList<Question>();
		List<Question> toReturn = new ArrayList<Question>();
		String theme = null;
		List<JobOffer> jobOffers = em.createQuery("Select j from JobOffer j ", JobOffer.class).getResultList();
		for(JobOffer j : jobOffers)
		   if(j.getJobOfferId() == JobOfferId) theme = j.getDomain();
		
		for(Question q : all)
		{
			String t = q.getTheme().toString();
			if (t.toLowerCase().equals(theme.toLowerCase())) 
				byTheme.add(q);
			if (t.toLowerCase().equals("Culture_générale".toLowerCase()))
				culture.add(q);
			if (t.toLowerCase().equals("Test_de_personnalité".toLowerCase()))
				personnalite.add(q);
		}
		
		int sizeTheme = byTheme.size();
		int sizecult = culture.size();
		int sizepers = personnalite.size();
		List<Integer> randomRes = new ArrayList<Integer>();
		Random rand = new Random();
		int r = rand.nextInt(sizeTheme);
		System.out.println("sizeTheme "+sizeTheme);
		System.out.println("sizecult "+sizecult);
		System.out.println("sizepers "+sizepers);
		for (int i=0;i<10;i++)
		{
			while (randomRes.contains(r)) 
			{
				System.out.println("random result "+r);
				r = rand.nextInt(sizeTheme);
			}
			randomRes.add(r);
			toReturn.add(byTheme.get(r));
			
		}
		r = rand.nextInt(sizecult);
		randomRes.clear();
		for (int i=0;i<2;i++)
		{
			while (randomRes.contains(r)) r = rand.nextInt(sizecult);
			randomRes.add(r);
			toReturn.add(culture.get(r));
			
		}
		r = rand.nextInt(sizepers);
		randomRes.clear();
		for (int i=0;i<3;i++)
		{
			while (randomRes.contains(r)) r = rand.nextInt(sizepers);
			randomRes.add(r);
			toReturn.add(personnalite.get(r));
			
		}
		
		return toReturn;
	}

	

	

}
