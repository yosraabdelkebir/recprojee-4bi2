package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import interfaces.CertificationServiceLocal;
import interfaces.CertificationServiceRemote;
import model.Certification;

@Stateless
@LocalBean
public class CertificationService implements CertificationServiceLocal, CertificationServiceRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public Certification DisplayCertification(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Certification> GetAllCertifofCandidate(int idC) {
		System.out.println("*******************  idC  "+idC+"  ********* B **********");
		TypedQuery<Certification> query =  em.createQuery("Select c from Certification c ", Certification.class);
		//query.setParameter("idC", idC); 
		System.out.println("*******************  idC  "+idC+"  ********* A **********");
		//List<Certification> certif = null; 
	/*	try {
			certif = query.getResultList();
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}*/
		return query.getResultList();
	}

}
