package services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.ContactServiceLocal;
import interfaces.ContactServiceRemote;
import model.*;

@Stateless
@LocalBean
public class ContactService implements ContactServiceLocal, ContactServiceRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@EJB
	CandidateService SCand;

	@Override
	public List<Contact> getAllContact() {
        List<Contact> contacts = em.createQuery("Select c from Contact c ", Contact.class).getResultList();
		return contacts;
	}
	
	@Override
	public List<Contact> getContactofCand(int id) {
		List<Contact> all = this.em.createQuery("Select c from Contact c ", Contact.class).getResultList();
		List<Contact> contacts = new ArrayList<>();
		for (Contact c : all)
		{
			//System.out.println("************ *********** ************* Contact Service ");
			//System.out.println("************ *********** ************* "+c.getId().getSenderId()+"  "+c.getId().getReceiverId());
			if (c.getId().getSenderId() == id || c.getId().getReceiverId() == id)
				contacts.add(c);
		}
		return contacts;
	}


	@Override
	public List<Candidate> getAmisCommun(int idX, int idY) {  
		List<Contact> contactsX = this.getContactofCand(idX); 
		List<Contact> contactsY = this.getContactofCand(idY);
		List<Candidate> communCandidates = new ArrayList<>();
		List<Contact> commun = new ArrayList<>();
		
		for (Contact cx : contactsX)
		  for (Contact cy : contactsY)
		  {
			  if ((cx.getId().getReceiverId() == cy.getId().getReceiverId()) ||
				  (cx.getId().getReceiverId() == cy.getId().getSenderId())   ||
			      (cx.getId().getSenderId() == cy.getId().getSenderId())     ||
			      (cx.getId().getSenderId() == cy.getId().getReceiverId()))
				  commun.add(cx);
		  }
		
		int idcand = 0;
		for (Contact c : commun)
			{
			   if (c.getId().getSenderId() == idX) idcand = c.getId().getReceiverId();
			   else
			   if (c.getId().getReceiverId() == idX) idcand = c.getId().getSenderId();
			   else
			   if (c.getId().getSenderId() == idY) idcand = c.getId().getReceiverId();
			   else
			   if (c.getId().getReceiverId() == idY) idcand = c.getId().getSenderId();
			   communCandidates.add(SCand.DisplayCandidate(idcand));
			}
		return communCandidates;
	}

	@Override
	public List<Candidate> getAmisDesAmis(int idC) {
		List<Candidate> amisDesAmis = new ArrayList<>();     // liste d'amis des amis du connecté
		List<Contact> contacts = this.getContactofCand(idC); // liste de contacts du connecté
		List<Contact> cOfc = new ArrayList<>();              // liste de contacts des contacts du connecté
		List<Integer> ids = new ArrayList<>();               // les id de contacts pour ne pas prendre l'id du connecté
		List<Integer> idcOfc = new ArrayList<>();  
		for (Contact c : contacts)
		{
			if (c.getId().getSenderId() == idC) ids.add(c.getId().getReceiverId());
			else
			if (c.getId().getReceiverId() == idC) ids.add(c.getId().getSenderId());
		}
		
		for (int i : ids)
		{
			for (Contact c : this.getContactofCand(i))
			{
				cOfc.add(c);
				if (c.getId().getSenderId() == i) idcOfc.add(c.getId().getReceiverId());
				if (c.getId().getReceiverId() == i) idcOfc.add(c.getId().getSenderId());
			}
		}
	
		for (int c : idcOfc)
		{
			amisDesAmis.add(SCand.DisplayCandidate(c));
		}
		int l = amisDesAmis.size();
		for (int x = 0;x<l;x++)
		{
			for (int i : ids)
		     {  
		      	if (amisDesAmis.get(x).getId() == i) 
		      		{
		      		  amisDesAmis.remove(amisDesAmis.get(x));
		      		  l--; x--;
		      		}
		     }
			if (amisDesAmis.get(x).getId() == idC )
			{
				amisDesAmis.remove(amisDesAmis.get(x));
	      		  l--; x--;
			}
		}
		
		return amisDesAmis;
	}

	

	

}
