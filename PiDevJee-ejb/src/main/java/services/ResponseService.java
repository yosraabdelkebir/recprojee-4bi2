package services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.ResponseServiceLocal;
import interfaces.ResponseServiceRemote;
import model.Response;

@Stateless
@LocalBean
public class ResponseService implements ResponseServiceLocal, ResponseServiceRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public int addResponse(Response r) {
		em.persist(r);
		return r.getResponseId();
	}

	@Override
	public List<Response> getResponseByQuestion(int QuestionId) {
		List<Response> all = em.createQuery("Select r from Response r ", Response.class).getResultList();
		List<Response> toReturn = new ArrayList<Response>();
		for(Response r : all)
			if (r.getQuestion().getQuestionId() == QuestionId)
				toReturn.add(r);
		return toReturn;
	}

	@Override
	public void updateResponse(Response r) {
		em.merge(r);
		System.out.println("Response updated !!!!!!! ");
		
	}

	@Override
	public void removeResponse(int id) {
		em.remove(em.find(Response.class, id));
		System.out.println("Response removed !!!!!!! ");
		
	}

}
