package services;

import javax.mail.PasswordAuthentication;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.OnLineTestServiceLocal;
import interfaces.OnLineTestServiceRemote;
import model.OnLineTest;

@Stateless
@LocalBean
public class OnLineTestService implements OnLineTestServiceLocal, OnLineTestServiceRemote {

	
	@PersistenceContext
	EntityManager em;
	

	

	
	@Override
	public List<OnLineTest> getNonPassedTestByCandidate(int candidateId){
		List<OnLineTest> all = em.createQuery("Select o from OnLineTest o ", OnLineTest.class).getResultList();
		List<OnLineTest> nonPassedByCand = new ArrayList<OnLineTest>();
		
		for (OnLineTest t : all)
			if (t.getCandidate().getId() == candidateId && t.getPassed() == 0) 
			{
				String dead=t.getDeadLine().substring(0, 10); 
				SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");  
				SimpleDateFormat formatter2=new SimpleDateFormat("yyyy-MM-dd");  
				SimpleDateFormat formatter3=new SimpleDateFormat("yyyy-MM-dd");  
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date dead1 = null;
				LocalDate nowld = LocalDate.now();
				Date now = null;
				try {   dead1=formatter1.parse(dead); 
		        		now=formatter2
		        				.parse(dateFormat.format(new Date()));}
				catch (ParseException e) {e.printStackTrace();}
				if(now.compareTo(dead1) <= 0)
					nonPassedByCand.add(t);
			}
		
		return nonPassedByCand;
	}


	@Override
	public void updateOnLineTest(OnLineTest e) {
		System.out.println("************************* update ************************* testId "+ e.getOnLineTestId() + "*************************");
		em.merge(e);
		System.out.println("************************* merge ************************* testId "+ e.getOnLineTestId() + "*************************");
	}


	@Override
	public OnLineTest getOnlineTest(int id) {
		List<OnLineTest> all = em.createQuery("Select o from OnLineTest o ", OnLineTest.class).getResultList();
		for (OnLineTest t : all)
			if (t.getOnLineTestId() == id)
				return t;
		return null;
	}


	@Override
	public void sendMail(String receiver,String text) {
       System.out.println("Preparing to send mail");
		
		Properties properties = new Properties();
		
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "25");
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		
		String myAccountEmail = "findmeajobonlineservices@gmail.com";
		String password = "findmeajob123online";
		
		System.out.println("*************************  **************"+ receiver +"***********  *************************");
		
		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myAccountEmail,password);
			}
		});
		
		Message message = prepareMessage(session,myAccountEmail,receiver,text);
		System.out.println("*************************  ************** ***********  *************************");
		
		try {
			Transport.send(message);
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		System.out.println("Message sent successfully");
		
	}
	
	private static Message prepareMessage(Session session , String myAccountEmail, String receiver, String text) {
		
		try {
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myAccountEmail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
			message.setSubject("OnLine test result");
			message.setText(text);
			System.out.println("*************************  ************** ***********  *************************");

			return message;
			
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return null;
		
	}
	
	

	
}
