package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the OnLineTest database table.
 * 
 */
@Entity
@NamedQuery(name="OnLineTest.findAll", query="SELECT o FROM OnLineTest o")
public class OnLineTest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="OnLineTestId")
	private int onLineTestId;

	@ManyToOne
	@JoinColumn(name="CandidateId")
	private Candidate candidate;

	@ManyToOne
	@JoinColumn(name="JobOfferId")
	private JobOffer jobOffer;

	@Column(name="Passed")
	private int passed;

	@Column(name="Scor")
	private Integer scor;
	
	@Column(name="DeadLine")
	private String deadLine;

	public OnLineTest() {
	}

	public int getOnLineTestId() {
		return this.onLineTestId;
	}

	public void setOnLineTestId(int onLineTestId) {
		this.onLineTestId = onLineTestId;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public JobOffer getJobOffer() {
		return jobOffer;
	}

	public void setJobOffer(JobOffer jobOffer) {
		this.jobOffer = jobOffer;
	}

	public int getPassed() {
		return this.passed;
	}

	public void setPassed(int passed) {
		this.passed = passed;
	}

	public Integer getScor() {
		return this.scor;
	}

	public void setScor(Integer scor) {
		this.scor = scor;
	}

	public String getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(String deadLine) {
		this.deadLine = deadLine;
	}
	
	

}