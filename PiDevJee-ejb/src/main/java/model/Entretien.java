package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Entretiens database table.
 * 
 */
@Entity
@Table(name="Entretiens")
@NamedQuery(name="Entretien.findAll", query="SELECT e FROM Entretien e")
public class Entretien implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EntretienId")
	private int entretienId;

	private String dateEntretien;

	//bi-directional many-to-one association to Candidature
	/*@ManyToOne
	@JoinColumn(name="CandidatureId")
	private Candidature candidature;*/

	public Entretien() {
	}

	public int getEntretienId() {
		return this.entretienId;
	}

	public void setEntretienId(int entretienId) {
		this.entretienId = entretienId;
	}

	public String getDateEntretien() {
		return this.dateEntretien;
	}

	public void setDateEntretien(String dateEntretien) {
		this.dateEntretien = dateEntretien;
	}

	/*public Candidature getCandidature() {
		return this.candidature;
	}

	public void setCandidature(Candidature candidature) {
		this.candidature = candidature;
	}*/

}