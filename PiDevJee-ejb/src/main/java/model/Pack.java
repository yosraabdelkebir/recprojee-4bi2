package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Packs database table.
 * 
 */
@Entity
@Table(name="Packs")
@NamedQuery(name="Pack.findAll", query="SELECT p FROM Pack p")
public class Pack implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PackId")
	private int packId;

	private float tauxReduction;

	public Pack() {
	}

	public int getPackId() {
		return this.packId;
	}

	public void setPackId(int packId) {
		this.packId = packId;
	}

	public float getTauxReduction() {
		return this.tauxReduction;
	}

	public void setTauxReduction(float tauxReduction) {
		this.tauxReduction = tauxReduction;
	}

}