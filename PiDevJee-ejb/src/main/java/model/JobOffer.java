package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the JobOffers database table.
 * 
 */
@Entity
@Table(name="JobOffers")
@NamedQuery(name="JobOffer.findAll", query="SELECT j FROM JobOffer j")
public class JobOffer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="JobOfferId")
	private int jobOfferId;

	private String domain;

	@OneToMany(mappedBy="jobOffer")
	private List<OnLineTest> onLineTests;

	public JobOffer() {
	}

	public int getJobOfferId() {
		return this.jobOfferId;
	}

	public void setJobOfferId(int jobOfferId) {
		this.jobOfferId = jobOfferId;
	}

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public List<OnLineTest> getOnLineTests() {
		return onLineTests;
	}

	public void setOnLineTests(List<OnLineTest> onLineTests) {
		this.onLineTests = onLineTests;
	}

	

}