package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Historiques database table.
 * 
 */
@Entity
@Table(name="Historiques")
@NamedQuery(name="Historique.findAll", query="SELECT h FROM Historique h")
public class Historique implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="HistoriqueId")
	private int historiqueId;

	public Historique() {
	}

	public int getHistoriqueId() {
		return this.historiqueId;
	}

	public void setHistoriqueId(int historiqueId) {
		this.historiqueId = historiqueId;
	}

}