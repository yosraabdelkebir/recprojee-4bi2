package ManagedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

import model.Etat;
import model.Question;
import model.Response;
import model.Theme;
import services.QuestionService;
import services.ResponseService;

@ManagedBean
@SessionScoped
public class QuestionBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int questionId;
	private String contenu;
	private Theme theme;
	private String stheme;
	private String type;
	private static List<Response> responses = new ArrayList<Response>();
	private List<Question> questions = new ArrayList<Question>();
	
	private int responseId;
	private String contenuR;
	private Etat etat;
	Question quest = null;
	private static Question QuestionToUp;
	
	@EJB
	QuestionService SQuest = null;
	@EJB
	ResponseService SResp = null;
	
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public Theme getTheme() {
		return theme;
	}
	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	
	public String getStheme() {
		return stheme;
	}
	public void setStheme(String stheme) {
		this.stheme = stheme;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Response> getResponses() {
		return responses;
	}
	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}
	
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public Question getQuest() {
		return quest;
	}
	public void setQuest(Question quest) {
		this.quest = quest;
	}
	public static Question getQuestionToUp() {
		return QuestionToUp;
	}
	public static void setQuestionToUp(Question questionToUp) {
		QuestionToUp = questionToUp;
	}
	public  List<Question> getQuestion() {
		questions = SQuest.getAllQuestions();
		return questions;
	}
	public void setQuestion(List<Question> questions) {
		this.questions = questions;
	}
	public int getResponseId() {
		return responseId;
	}
	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}
	public String getContenuR() {
		return contenuR;
	}
	public void setContenuR(String contenuR) {
		this.contenuR = contenuR;
	}
	public Etat getEtat() {
		return etat;
	}
	public void setEtat(Etat etat) {
		this.etat = etat;
	}
	
/*	public List<Response> getAnswers() {
		return answers;
	}
	public void setAnswers(List<Response> answers) {
		this.answers = answers;
	}
*/
	public void addNewAnswer(String c,Etat e)
	{
		System.out.println("addNewAnswer() "+c+" "+e);
		this.getResponses().add(new Response(c,e,null));
		System.out.println("responses " +this.getResponses().size());
	}
	
	public void ajoutQuestion()
	{
		quest = SQuest.addQuestion(new Question(contenu,theme,type));
		//System.out.println("responses " +responses.size());
		for(Response r : responses)
		{
			System.out.println("addNewAnswer() "+r.getContenu()+" "+r.getEtat());
			  r.setQuestion(quest);
			  SResp.addResponse(r);
		}
		responses = new ArrayList<Response>(); 
		System.out.println("responses " +responses.size());
	}
	
	public String suppQuestion(int id)
	{   System.out.println("*************** suppQuestion ************* " +id+" *************");
		SQuest.removeQuestion(id);
	    System.out.println("*************** Question "+id+" ************* supprimée *************");
		return "Supression";
	}
	
	public String modifyQandAPage(Question q) 
	{
		QuestionToUp = new Question();
		String navigateTo = "null"; 
		System.out.println("*************** Question to update ************* " +q.getQuestionId()+" *************");
		QuestionToUp.setQuestionId(q.getQuestionId());
		QuestionToUp.setContenu(q.getContenu());
		QuestionToUp.setTheme(q.getTheme());
		ResponseBean.answers= SResp.getResponseByQuestion(q.getQuestionId());
		System.out.println("*************** navigate to modify Q and A ************* nb answers "+ResponseBean.answers);
		navigateTo = "/pages/quiz/modifyQandA.xhtml?faces-redirect=true"; 
		return navigateTo;
	}
	
	public void modifyResponses(Response answer)
	{
		System.out.println("*************** public void modifyResponses(Response answer) ************* ");
		for(Response r : ResponseBean.answers)
			if (r.getResponseId() == answer.getResponseId())
			{
				r.setContenu(answer.getContenu());
				r.setEtat(answer.getEtat());
			}
	}
	
	public String modifQuestion()
	{
		Question q = new Question();
		q.setQuestionId(QuestionToUp.getQuestionId());
		q.setContenu(QuestionToUp.getContenu());
		q.setTheme(QuestionToUp.getTheme());
		System.out.println("*************** updateQuestion ************* " +q.getQuestionId()+" *************");
		SQuest.updateQuestion(q);
		System.out.println("*************** Question "+q.getQuestionId()+" ************* updated *************");
		for(Response r : ResponseBean.answers)
			SResp.updateResponse(r);
		ResponseBean.answers = new ArrayList<Response>();
		return "/pages/quiz/QandR.xhtml?faces-redirect=true"; 
	}
	
	public String removeAnswer(Response answer)
	{
		SResp.removeResponse(answer.getResponseId());
		return "/pages/quiz/QandR.xhtml?faces-redirect=true"; 
	}
	
}
