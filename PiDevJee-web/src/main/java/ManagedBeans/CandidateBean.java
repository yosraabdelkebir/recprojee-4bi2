package ManagedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Candidate;

import javax.ejb.EJB;

import services.*;

@ManagedBean(name = "candidateBean") 
@SessionScoped
public class CandidateBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int scor = 0;
	public static int scorR = 0;
	
	
	private int id;
	private int act;
	private String address;
	private String creationDate;
	private String email;
	private String firstName;
	private String introduction;
	private String lastName;
	private String password;
	private String phoneNumber;
	private String photo;
	private String skills;
	private String userName;
	private Candidate candidat;
	public static Candidate candidatconnect = null;
	private List<Candidate> allCandidates = new ArrayList<>();
	private List<Candidate> fOFf = new ArrayList<>();
	
	@EJB
	CandidateService SCand;
	@EJB
	CertificationService SCertif = null;
	@EJB
	ContactService SContact = null;
	
	public CandidateBean() {
		super();
		this.setCandidat(candidatconnect);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAct() {
		return act;
	}

	public void setAct(int act) {
		this.act = act;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Candidate getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidate candidat) {
		this.candidat = candidat;
	}

	
	public static Candidate getCandidatconnect() {
		return candidatconnect;
	}

	public static void setCandidatconnect(Candidate candidatconnect) {
		CandidateBean.candidatconnect = candidatconnect;
	}

	public Candidate getCandidate(int id) {
		candidat = SCand.DisplayCandidate(id);
		System.out.println("candidat candidateBean "+candidat.getEmail());
		return candidat;
	}

	public List<Candidate> getAllCandidates() {
		return allCandidates = new ArrayList<Candidate>(SCand.getAllCandidate());
	}

	public void setAllCandidates(List<Candidate> allCandidates) {
		this.allCandidates = allCandidates;
	}

	public List<Candidate> getfOFf() {
		return SContact.getAmisDesAmis(CandidateBean.candidatconnect.getId());
	}

	public void setfOFf(List<Candidate> fOFf) {
		this.fOFf = fOFf;
	}

	public static int getScor() {
		return scor;
	}

	public static void setScor(int scor) {
		CandidateBean.scor = scor;
	} 
	
	
	
	

}
