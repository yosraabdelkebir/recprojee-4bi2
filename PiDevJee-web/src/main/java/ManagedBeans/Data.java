package ManagedBeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import model.Etat;
import model.Theme;

@ManagedBean
@ApplicationScoped
public class Data {

	public Theme[] getThemes()
	{
		return Theme.values();
	}
	
	public Etat[] getEtats()
	{
		return Etat.values();
	}
	
}
