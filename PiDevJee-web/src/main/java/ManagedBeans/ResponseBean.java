package ManagedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

import model.Etat;
import model.Question;
import model.Response;
import services.ResponseService;

@ManagedBean
@SessionScoped
public class ResponseBean implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int responseId;
	private String contenu;
	private String etat;
	private Question question;
	public static List<Response> answers = new ArrayList<Response>();
	
	public static String contenuR;
	public static Etat etatR;
	
	@EJB
	ResponseService SResp;
	
	public int getResponseId() {
		return responseId;
	}
	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	
	public static List<Response> getAnswers() {
		return answers;
	}
	public static void setAnswers(List<Response> answers) {
		ResponseBean.answers = answers;
	}
	public List<Response> getAnswerByQuestion(int QuestionId)
	{
		return SResp.getResponseByQuestion(QuestionId);	
	}
	public static String getContenuR() {
		return contenuR;
	}
	public static void setContenuR(String contenuR) {
		ResponseBean.contenuR = contenuR;
	}
	public static Etat getEtatR() {
		return etatR;
	}
	public static void setEtatR(Etat etatR) {
		ResponseBean.etatR = etatR;
	}
	
	
	
}
