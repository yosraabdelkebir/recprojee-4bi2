package ManagedBeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.OneToMany;

import services.*;
import model.*;

import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class OnLineTestBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<OnLineTest> testNonPassed = new ArrayList<>();
	private static int JobOfferId;
	private static int onLineTestId;
	
	
	
	@EJB
	OnLineTestService STest = null;
	
	@EJB
	QuizService SQuiz = null;

	@EJB
	ResponseService SResp = null;
	
	@EJB
	CandidateService SCand = null;


	public List<OnLineTest> getTestNonPassed() {
		testNonPassed = new ArrayList<>(STest.getNonPassedTestByCandidate(LoginBean.idconn));
		return testNonPassed;
	}

	public void setTestNonPassed(List<OnLineTest> testNonPassed) {
		this.testNonPassed = testNonPassed;
	}
	
	
	
	public static int getJobOfferId() {
		return JobOfferId;
	}

	public static void setJobOfferId(int jobOfferId) {
		JobOfferId = jobOfferId;
	}

	public static int getOnLineTestId() {
		return onLineTestId;
	}

	public static void setOnLineTestId(int onLineTestId) {
		OnLineTestBean.onLineTestId = onLineTestId;
	}

	public String passTest(int onLineTestId,JobOffer jobOff)
	{
		JobOfferId = jobOff.getJobOfferId();
		OnLineTestBean.onLineTestId = onLineTestId;
		String navigateTo = "/pages/quiz/passQuiz?faces-redirect=true";;
		return navigateTo;
	}
	
	public List<Question> generateQuiz ()
	{   int nbcorrect = 0;
	    List<Question> quiz = SQuiz.createQuiz(JobOfferId);
		for(Question q : quiz)
			for (Response r : SResp.getResponseByQuestion(q.getQuestionId()))
				if (r.getEtat().toString().equals("Correct")) nbcorrect++;
		CandidateBean.scorR = nbcorrect*10;
		return quiz;
	}
	
	public void calculScor(Response answer)
	{
		System.out.println("************************* answer ************************* "+ answer.getEtat().toString() + "*************************");
		if (answer.getEtat().toString().equals("Correct")) 
			CandidateBean.scor+=10;
		System.out.println("************************* SCOR ************************* "+ CandidateBean.scor + "*************************");
	}
	
	public String submitQuiz (int testId)
	{
		System.out.println("************************* submitQuiz ************************* testId "+ testId + "*************************");
		OnLineTest o = STest.getOnlineTest(testId);
		o.setPassed(1);
		o.setScor(CandidateBean.scor);
		STest.updateOnLineTest(o);
		float scoraccepted = (CandidateBean.scorR * 80 )/100;
		String mail = "";
		if (CandidateBean.scor >= scoraccepted) 
			  mail = "Vous avez bien passer le test en ligne concernant l'offre numéro "+o.getJobOffer().getJobOfferId()+".</br>"+
		             "vous serez informer par la date d'entretien ultérieurement.";
		     mail = "Vous avez mal passer le test en ligne concernant le job offer numéro "+o.getJobOffer().getOnLineTests()+"."+
	                 "N'hésitez pas de postuler dans d'autres Offres";
		System.out.println("*************************  ***********"+LoginBean.idconn+"**************  *************************");
		STest.sendMail(SCand.DisplayCandidate(LoginBean.idconn).getEmail(), mail);
		System.out.println("*************************  *************************  *************************");
		CandidateBean.scor = 0;
		return "/pages/candidate/profilCandidate?faces-redirect=true";
	}
	
	public Void passOnLineTest(int testId) {
		return null;
		
	}

}
